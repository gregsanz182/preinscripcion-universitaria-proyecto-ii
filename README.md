![Logo UNET](unetLogo.png "Logo de la UNET")

"Preinscripción Universitaria"
==============================

### Proyecto II de Programación I (C++)

Segundo proyecto de Programación I. Consiste en un sistema para la preinscripción de los estudiantiles.
Programado en lenguaje C++.

Proyecto desarrollado en Microsoft Visual Studio.

Para más información revisar el enunciado "I Proyecto 2012-2.pdf"

#### Desarrolladores
* [Anny Chacón (@AnnyChacon)](https://github.com/AnnyChacon)
* [Gregory Sánchez (@gregsanz182)](https://github.com/gregsanz182)

#### Asignatura
* Nombre: Programación I
* Código: 0416202T
* Profesor: Manuel Sánchez

*Proyecto desarrollado con propositos educativos para la **Universidad Nacional
Experimental del Táchira***
