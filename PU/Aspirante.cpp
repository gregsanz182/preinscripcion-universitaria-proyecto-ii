#include "StdAfx.h"
#include "Aspirante.h"
#include <fstream>

Aspirante::Aspirante()
{
	nombre = "";
	cedula = "";
	telefono = "";
	edad = 0;
	carrera = 0;
	direccion = "";
	estado = 0;
	aula = "";
}


Aspirante::~Aspirante()
{
}

void Aspirante::leerDatos(){
	cout << "Nombre completo: ";
	cin.sync();
	getline(cin, nombre);
	cout << "Cedula: ";
	cin.sync();
	getline(cin, cedula);
	cout << "Telefono de ubicacion: ";
	cin.sync();
	getline(cin, telefono);
	cout << "Edad: ";
	cin >> edad;
	do{
		cout << "Carrera a preinscribir: "<< endl
			<< "1.Informatica." << endl
			<< "2.Electronica." << endl
			<< "3.Industrial." << endl
			<< "4.Mecanica." << endl
			<< "5.Ambiental. " << endl
			<< "6.Produccion Animal." << endl
			<< "7.Agronomia." << endl
			<< "8.Contruccion Civil." << endl
			<< "9.Musica." << endl
			<< "10.Electro Medicina. " << endl
			<< "Opcion: ";
		cin >> carrera;
	}while(!(carrera >0 && carrera<11));
	cout << "Lugar de procedencia: ";
	cin.sync();
	getline(cin, direccion);
}

void Aspirante::mostrarDatos(){
	cout << Nombre() << ":" << nombre << ":" << cedula << ":" << telefono << ":" << edad << ":" << Carrera() << ":" << direccion << ":" << aula << ":" << estado << ":";
}

void Aspirante::setEstado(int estado){
	this->estado = estado;
}

void Aspirante::setAula(string aula){
	this->aula = aula;
}

void Aspirante::respaldar(ofstream &out){
	out << tipo() << " " << nombre << "#" << cedula << " " << telefono << " " << edad << " " << carrera << " " << direccion << "#" << aula << " " << estado << " ";
}

void Aspirante::relevantes(ofstream &out){
	out << Nombre() << " - " << nombre << " - " << cedula << " - " << telefono << " - " << edad << " - " << Carrera() << " - " << direccion << endl;
}

void Aspirante::recuperar(ifstream &in){
	in.get();
	getline(in, nombre, '#');
	in >> cedula >> telefono >> edad >> carrera;
	in.get();
	getline(in, direccion, '#');
	in >> aula;
	in.get();
	in >> estado;
}

void Aspirante::procesar(){
	if(requisito())
		estado = 1;
}

string Aspirante::Carrera(){
	string car;
	switch(carrera){
	case Informatica: car="Informatica"; break;
	case Electronica: car="Electronica"; break;
	case Industrial: car="Industrial"; break;
	case Mecanica: car="Mecanica"; break;
	case ProduccionA: car ="Produccion Animal"; break;
	case Musica: car="Musica"; break;
	case ElectroMedicina: car="Electromedicina"; break;
	case ContruccionC: car="Contruccion Civil"; break;
	case Ambiental: car="Ambiental"; break;
	case Agronomia: car="Agronomia"; break;
	}
	return car;
}