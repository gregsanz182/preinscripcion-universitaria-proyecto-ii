#pragma once
#include "Aspirante.h"
#include <string>
#include <iostream>

using namespace std;

class ReadmisionRR : public Aspirante {
public:
	ReadmisionRR(void);
	~ReadmisionRR(void);
	void leerDatos();
	void mostrarDatos();
	int tipo(){ return 2; }
	bool requisito();
	void respaldar(ofstream &out);
	void recuperar(ifstream &in);
	string Nombre(){ return "ReadmisionRR"; }
private:
	string ultimoPeriodo;
};

