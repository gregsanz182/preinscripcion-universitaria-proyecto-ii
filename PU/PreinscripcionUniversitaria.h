#pragma once
#include "Aspirante.h"
#include "NuevoIngreso.h"
#include "ReadmisionCS.h"
#include "ReadmisionRR.h"
#include "ReadmisionRV.h"
#include <string>
#include <iostream>

#define MAX_ASPI 9000
#define MAX_CA 39

using namespace std;

class PreinscripcionUniversitaria
{
public:
	PreinscripcionUniversitaria(void);
	~PreinscripcionUniversitaria(void);
	void menu();
	void registrarAspirante();
	void procesarAspirante();
	Aspirante* buscarCedula(string cedula);
	void listadoAspirantesAptos();
	void listadoAspirantesPreinscritos();
	void RespaldaPorCedula();
	void recuperarInformacion();
	void cantAspirantesCarrera();
	void destruir();
	void mostrarPorcentaje(int opc);
private:
	Aspirante **aspirantes;
	int nAspirante;
	int aulaActual;
};

