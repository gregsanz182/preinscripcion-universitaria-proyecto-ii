#pragma once
#include "Aspirante.h"
#include <string>

using namespace std;

class NuevoIngreso : public Aspirante {
public:
	NuevoIngreso(void);
	~NuevoIngreso(void);
	void leerDatos();
	void mostrarDatos();
	int tipo(){ return 0; }
	float getpromedios();
	bool requisito();
	void respaldar(ofstream &out);
	void recuperar(ifstream &in);
	void destruirProme();
	string Nombre(){ return "Nuevo Ingreso"; }
private:
	string liceoProv;
	float *promedios;
	string titulo;
};

