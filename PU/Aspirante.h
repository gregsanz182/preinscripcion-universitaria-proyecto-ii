#pragma once
#include <string>
#include <iostream>

enum CARRERAPU { Defecto, Informatica, Electronica, Industrial, Mecanica, Ambiental, ProduccionA, Agronomia, ContruccionC, Musica, ElectroMedicina};

using namespace std;

class Aspirante
{
public:
	Aspirante();
	~Aspirante();
	virtual void leerDatos();
	virtual void mostrarDatos();
	virtual int tipo()=0;
	virtual bool requisito()=0;
	int getEstado(){ return estado; }
	void setEstado(int estado);
	string getCedula(){ return cedula; }
	void setAula(string aulaActual);
	string getAula(){ return aula; }
	int getCarrera(){ return carrera; }
	string getNombre(){ return nombre; }
	virtual void respaldar(ofstream &out);
	void relevantes(ofstream &out);
	virtual void recuperar(ifstream &in);
	virtual string Nombre()=0;
	void procesar();
	string Carrera();
private:
	string nombre;
	string cedula;
	string telefono;
	int edad;
	int carrera;
	string direccion;
	int estado;
	string aula;
};

