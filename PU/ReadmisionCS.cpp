#include "StdAfx.h"
#include "ReadmisionCS.h"
#include <fstream>


ReadmisionCS::ReadmisionCS(void): Aspirante()
{
	promedioAct = 0.0;
	carrera = 0;
}


ReadmisionCS::~ReadmisionCS(void)
{
}

void ReadmisionCS::leerDatos(){
	Aspirante::leerDatos();
	cout << "Indique su Promedio Actual: ";
	cin >> promedioAct;
	do{
		cout << "Carrera a preinscribir: "<< endl
			<< "1.Informatica." << endl
			<< "2.Electronica." << endl
			<< "3.Industrial." << endl
			<< "4.Mecanica." << endl
			<< "5.Ambiental. " << endl
			<< "6.Produccion Animal." << endl
			<< "7.Agronomia." << endl
			<< "8.Contruccion Civil." << endl
			<< "9.Musica." << endl
			<< "10.Electro Medicina. " << endl
			<< "Opcion: ";
		cin >> carrera;
	}while(!(carrera >0 && carrera<11));
}

void ReadmisionCS::mostrarDatos(){
	Aspirante::mostrarDatos();
	string car;
	switch(carrera){
	case Informatica: car="Informatica"; break;
	case Electronica: car="Electronica"; break;
	case Industrial: car="Industrial"; break;
	case Mecanica: car="Mecanica"; break;
	case ProduccionA: car ="ProduccionA"; break;
	case Musica: car="Musica"; break;
	case ElectroMedicina: car="ElectroMedicina"; break;
	case ContruccionC: car="ContruccionC"; break;
	case Ambiental: car="Ambiental"; break;
	case Agronomia: car="Agronomia"; break;
	}
	cout << promedioAct << ":" << car << endl;
}

bool ReadmisionCS::requisito(){
	if(promedioAct > 15 && carrera != Aspirante::getCarrera()){
		return true;
	}else{
		return false;
	}
}

void ReadmisionCS::respaldar(ofstream &out){
	Aspirante::respaldar(out);
	out << promedioAct << " " << carrera << endl;
}

void ReadmisionCS::recuperar(ifstream &in){
	Aspirante::recuperar(in);
	in.get();
	in >> promedioAct >> carrera;
}