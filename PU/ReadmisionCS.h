#pragma once
#include "Aspirante.h"
#include <string>
#include <iostream>

using namespace std;

class ReadmisionCS : public Aspirante {
public:
	ReadmisionCS(void);
	~ReadmisionCS(void);
	void leerDatos();
	void mostrarDatos();
	int tipo(){ return 1; }
	bool requisito();
	float getPromedioAct(){ return promedioAct; }
	int getCarrera(){ return carrera; }
	void respaldar(ofstream &out);
	void recuperar(ifstream &in);
	string Nombre(){ return "ReadmisionCS"; }
private:
	float promedioAct;
	int carrera;
};

