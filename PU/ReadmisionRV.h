#pragma once
#include "Aspirante.h"
#include <string>
#include <iostream>

using namespace std;

class ReadmisionRV : public Aspirante {
public:
	ReadmisionRV(void);
	~ReadmisionRV(void);
	void leerDatos();
	void mostrarDatos();
	int tipo(){ return 3; }
	bool requisito();
	string getFechaRetiro(){ return fechaRetiro; }
	void respaldar(ofstream &out);
	void recuperar(ifstream &in);
	string Nombre(){ return "ReadmisionRV"; }
private:
	string fechaRetiro;
};

