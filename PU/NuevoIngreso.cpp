#include "StdAfx.h"
#include "NuevoIngreso.h"
#include <fstream>

NuevoIngreso::NuevoIngreso(void): Aspirante()
{
	liceoProv = "";
	promedios = new float[5];
	titulo = "";
}


NuevoIngreso::~NuevoIngreso(void)
{
	destruirProme();
}

void NuevoIngreso::destruirProme(){
	if(promedios != NULL)
		delete[] promedios;
}

void NuevoIngreso::leerDatos(){
	Aspirante::leerDatos();
	cout << "Liceo donde obtuvo el titulo de bachiller: ";
	cin.sync();
	getline(cin, liceoProv);
	for(int i = 0; i < 5; i++){
		cout << "Promedio de notas del a�o " << (i+1) << ": ";
		cin >> promedios[i];
	}
	cout << "Titulo obtenido: ";
	cin.sync();
	getline(cin, titulo);
}

void NuevoIngreso::mostrarDatos(){
	Aspirante::mostrarDatos();
	cout << liceoProv << ":";
	for(int i = 0; i < 5; i++){
		cout << promedios[i] << ":";
	}
	cout << titulo << endl;
}

float NuevoIngreso::getpromedios(){
	float prome = 0.0;
	for(int i=0; i<5; i++){
		prome += promedios[i];
	}
	prome /= 5;
	return prome;
}

bool NuevoIngreso::requisito(){
	float opc;
	opc = getpromedios();
	if(opc > 12){
		return true;
	}else{
		return false;
	}
}

void NuevoIngreso::respaldar(ofstream &out){
	Aspirante::respaldar(out);
	out << liceoProv << "#";
	for(int i = 0; i < 5; i++){
		out << promedios[i] << " ";
	}
	out << titulo << endl;
}

void NuevoIngreso::recuperar(ifstream &in){
	destruirProme();
	promedios = new float[5];
	Aspirante::recuperar(in);
	in.get();
	getline(in, liceoProv, '#');
	for(int i=0; i<5; i++){
		in >> promedios[i];
	}
	in.get();
	getline(in, titulo);
}