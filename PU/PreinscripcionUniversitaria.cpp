#include "StdAfx.h"
#include "PreinscripcionUniversitaria.h"
#include <sstream>
#include <fstream>

PreinscripcionUniversitaria::PreinscripcionUniversitaria(void)
{
	aspirantes = new Aspirante*[MAX_ASPI];
	nAspirante = 0;
	aulaActual = 1;
}


PreinscripcionUniversitaria::~PreinscripcionUniversitaria(void)
{
	destruir();
}

void PreinscripcionUniversitaria::destruir(){
	for(int i=0; i<nAspirante; i++)
		delete aspirantes[i];
	if(aspirantes != NULL){ delete[] aspirantes; }
}

void PreinscripcionUniversitaria::menu(){
	int resp;
	do{
		cout << endl << "**************** MENU ****************" << endl << endl
			<< "1. Registar solicitud de Aspirante." << endl
			<< "2. Cantidad de apirantes por carrera." << endl
			<< "3. Consultar solicitud de aspirante." << endl
			<< "4. Generar archivo de aspirantes aptos para ser admitidos." << endl
			<< "5. Generar archivo de aspirantes Preinscritos." << endl
			<< "6. Respaldar informacion de aspirantes por cedula." << endl
			<< "7. Recuperar Infomacion de Aspirantes." << endl
			<< "Opcion: ";
		cin >> resp;
		switch(resp){
		case 1:
			registrarAspirante();
			break;
		case 2:
			cantAspirantesCarrera();
			break;
		case 3:
			procesarAspirante();
			break;
		case 4:
			listadoAspirantesAptos();
			break;
		case 5:
			listadoAspirantesPreinscritos();
			break;
		case 6:
			RespaldaPorCedula();
			break;
		case 7:
			recuperarInformacion();
			break;
		default:
			cout << "Opcion incorrecta..." << endl;
			break;
		}
	}while(resp >0 && resp <8);
}

void PreinscripcionUniversitaria::registrarAspirante(){
	int opc;
	Aspirante *aspi = NULL;
	do{
		cout << endl << "Indique el tipo de Preinscripcion" << endl
			<< "1. Nuevo Ingreso." << endl
			<< "2. ReadmisionCS." << endl
			<< "3. ReadmisionRR." << endl
			<< "4. ReadmisionRV." << endl
			<< "Opcion: ";
		cin >> opc;
		cout << endl;
	}while(opc<0 && opc>=4);
	switch(opc){
	case 1:
		aspi = new NuevoIngreso();
		break;
	case 2:
		aspi = new ReadmisionCS();
		break;
	case 3:
		aspi = new ReadmisionRR();
		break;
	case 4:
		aspi = new ReadmisionRV();
		break;
	}
	aspi->leerDatos();
	aspi->procesar();
	ostringstream au;
	aulaActual = (nAspirante/40) + 1;
	au << "A" << aulaActual;
	aspi->setAula(au.str());
	aspirantes[nAspirante++] = aspi;
	cout << endl << "Solicitud de Aspirante creada exitosamente..." << endl;
}



void PreinscripcionUniversitaria::procesarAspirante(){
	string cedula;
	cout << endl << "Indique la cedula del aspirante que desea procesar: ";
	cin >> cedula;
	cout << endl;
	Aspirante* aspi = buscarCedula(cedula);
	if(aspi == NULL){
		cout << "La cedula que ingreso no esta registrada..." << endl;
	}else{
		aspi->mostrarDatos();
		cout << endl << (aspi->getEstado() == 1?"Aspirante APTO":"Aspirante NO apto") << endl;
	}
}

Aspirante* PreinscripcionUniversitaria::buscarCedula(string cedula){
	for(int i=0; i<nAspirante; i++){
		if(cedula == aspirantes[i]->getCedula()){
			return aspirantes[i];
		}
	}
	return NULL;
}

void PreinscripcionUniversitaria::listadoAspirantesAptos(){
	ofstream out("AspirantesAptos.txt");
	for(int i=0; i<nAspirante; i++){
		if(aspirantes[i]->getEstado() == 1){
			aspirantes[i]->relevantes(out);
		}
	}
	cout << endl << "Archivo creado exitosamente como  <AspitantesAptos.txt>" << endl << endl;
}

void PreinscripcionUniversitaria::listadoAspirantesPreinscritos(){
	int k=0;
	ofstream out("Preinscritos.txt");
	while(k<4){
		for(int j=0; j<nAspirante; j++){
			if(aspirantes[j]->tipo() == k){
				out << aspirantes[j]->Nombre() << " - " << aspirantes[j]->getNombre() << " - " << aspirantes[j]->getAula() << endl;
			}
		}
		k++;
	}
	cout << "Archivo creado exitosamente como  <Preinscritos.txt>" << endl << endl;
}

void PreinscripcionUniversitaria::RespaldaPorCedula(){
	Aspirante *aux;
	for(int i=1; i<nAspirante; i++){
		for(int j=0; j<i; j++){
			if(aspirantes[j]->getCedula() > aspirantes[i]->getCedula()){
				aux = aspirantes[i];
				aspirantes[i] = aspirantes[j];
				aspirantes[j] = aux;
			}
		}
	}
	ofstream out("PreinscripcionUniversitaria.txt");
	for(int k=0; k<nAspirante; k++){
		aspirantes[k]->respaldar(out);
	}
	out.close();
}
void PreinscripcionUniversitaria::cantAspirantesCarrera(){
	int opc;
	do{
		cout << endl << "Porcentaje: "<< endl
			<< "0.Salir." << endl
			<< "1.Informatica." << endl
			<< "2.Electronica." << endl
			<< "3.Industrial." << endl
			<< "4.Mecanica." << endl
			<< "5.Ambiental. " << endl
			<< "6.Produccion Animal." << endl
			<< "7.Agronomia." << endl
			<< "8.Contruccion Civil." << endl
			<< "9.Musica." << endl
			<< "10.Electro Medicina. " << endl
			<< "Opcion: ";
		cin >> opc;
		if(opc >0 && opc <11)
			mostrarPorcentaje(opc);
	}while(opc!=0);
}

void PreinscripcionUniversitaria::mostrarPorcentaje(int opc){
	int cont=0;
	for(int i=0; i<nAspirante;i++){
		if(aspirantes[i]->getCarrera() == opc){
			cont++;
		}
	}
	cout << endl << "Porcentaje de Carrera Seleccionada: " << (((float)cont)*100)/nAspirante << "%" << endl;
}

void PreinscripcionUniversitaria::recuperarInformacion(){
	destruir();
	aspirantes = new Aspirante*[MAX_ASPI];
	nAspirante = 0;
	ifstream in("PreinscripcionUniversitaria.txt");
	Aspirante* aspi = NULL;
	int tipo;
	string archivo;
	if(in.fail()){
		cout << endl << "Error al abrir el archivo..." << endl;
	}else{
		in >> tipo;
		while(!in.eof()){
			in.clear();
			switch(tipo){
			case 0:
				aspi = new NuevoIngreso();
				break;
			case 1:
				aspi = new ReadmisionCS();
				break;
			case 2:
				aspi = new ReadmisionRR();
				break;
			case 3:
				aspi = new ReadmisionRV();
				break;
			}
			aspi->recuperar(in);
			aspirantes[nAspirante++] = aspi;
			in >> tipo;
		}
	}
	in.close();
}