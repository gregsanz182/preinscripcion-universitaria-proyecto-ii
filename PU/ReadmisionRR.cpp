#include "StdAfx.h"
#include "ReadmisionRR.h"
#include <fstream>


ReadmisionRR::ReadmisionRR(void): Aspirante()
{
	ultimoPeriodo = "";
}


ReadmisionRR::~ReadmisionRR(void)
{
}

void ReadmisionRR::leerDatos(){
	Aspirante::leerDatos();
	cout << "Indique el ultimo periodo cursado de la siguiente manera (2000-1): ";
	cin >> ultimoPeriodo;
}

void ReadmisionRR::mostrarDatos(){
	Aspirante::mostrarDatos();
	cout << ultimoPeriodo << endl;
}

bool ReadmisionRR::requisito(){
	if(ultimoPeriodo > "2007-1" && ultimoPeriodo < "2010-2"){
		return true;
	}else{
		return false;
	}
}

void ReadmisionRR::respaldar(ofstream &out){
	Aspirante::respaldar(out);
	out << ultimoPeriodo << endl;
}

void ReadmisionRR::recuperar(ifstream &in){
	Aspirante::recuperar(in);
	in.get();
	in >> ultimoPeriodo;
}