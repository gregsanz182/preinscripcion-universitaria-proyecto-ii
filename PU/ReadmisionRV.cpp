#include "StdAfx.h"
#include "ReadmisionRV.h"
#include <fstream>

ReadmisionRV::ReadmisionRV(void): Aspirante()
{
	fechaRetiro = "";
}


ReadmisionRV::~ReadmisionRV(void)
{
}

void ReadmisionRV::leerDatos(){
	Aspirante::leerDatos();
	cout << "Fecha de Retiro: ";
	cin >> fechaRetiro;
}

void ReadmisionRV::mostrarDatos(){
	Aspirante::mostrarDatos();
	cout << fechaRetiro << endl;
}

bool ReadmisionRV::requisito(){
	if(fechaRetiro > "2012"){
		return true;
	}else{
		return false;
	}
}

void ReadmisionRV::respaldar(ofstream &out){
	Aspirante::respaldar(out);
	out << fechaRetiro << endl;
}

void ReadmisionRV::recuperar(ifstream &in){
	Aspirante::recuperar(in);
	in.get();
	in >> fechaRetiro;
}